(function () {

    const PREF_SCHEME = 'media';
    const LIGHT_SCHEME = 'light';
    const DARK_SCHEME = 'dark';
    const COLOR_SCHEME_PREFS = 'colorScheme';
    const COLOR_SCHEME_MQ = '(prefers-color-scheme)';
    const DARK_SCHEME_MQ = '(prefers-color-scheme: dark)';
    const schemeSwitch = document.getElementById('scheme-switch');
    const schemeToggle = document.getElementById('scheme-toggle');
    const toggleWidget = document.getElementById('toggle-widget');
    const colorSchemePrefs = localStorage.getItem(COLOR_SCHEME_PREFS) || { name: PREF_SCHEME };

    const setColorScheme = (checked, scheme) => {
        schemeSwitch.checked = checked;
        if (scheme) {
            document.body.dataset.colorScheme = scheme;
        }
        if (document.contains(schemeToggle)) {
            schemeToggle.checked = (isPrefScheme()) ? true : false;
        }
        colorSchemePrefs.switch = schemeSwitch.checked;
        colorSchemePrefs.toggle = schemeToggle.checked;
        colorSchemePrefs.name = document.body.dataset.colorScheme;
        localStorage.setItem(COLOR_SCHEME_PREFS, JSON.stringify(colorSchemePrefs));
    }

    const isPrefScheme = () => {
        return document.body.dataset.colorScheme === PREF_SCHEME;
    }

    schemeSwitch.addEventListener('change', (event) => {
        setColorScheme(event.target.checked, (event.target.checked) ? LIGHT_SCHEME : DARK_SCHEME);
    });

    if (window.matchMedia && window.matchMedia(COLOR_SCHEME_MQ).media !== 'not all') {
        const darkSchemeMediaQuery = window.matchMedia(DARK_SCHEME_MQ);
        const colorSchemeMediaQueryListener = (event) => {
            if (isPrefScheme()) {
                setColorScheme((event.matches) ? false : true);
            }
        }
        if (darkSchemeMediaQuery.addEventListener) {
            darkSchemeMediaQuery.addEventListener('change', colorSchemeMediaQueryListener);
        } else if (darkSchemeMediaQuery.addListener) {
            darkSchemeMediaQuery.addListener(colorSchemeMediaQueryListener);
        }
        schemeToggle.addEventListener('change', (event) => {
            if (event.target.checked) {
                setColorScheme(!darkSchemeMediaQuery.matches, PREF_SCHEME);
            } else {
                setColorScheme(schemeSwitch.checked, (schemeSwitch.checked) ? LIGHT_SCHEME : DARK_SCHEME);
            }
        });
        if (isPrefScheme()) {
            setColorScheme(!darkSchemeMediaQuery.matches);
        }
    } else {
        toggleWidget.remove();
    }

})();