const express = require('express')
const engines = require('consolidate')
const apps = require('multilane')
const config = require('./config.js')

const site = apps.express(express(), engines, config)

const PORT = process.env.PORT || 8080
site.listen(PORT, () => {
    console.log(`http://localhost:${PORT}/`)
})

module.exports = site