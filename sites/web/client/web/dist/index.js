"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _react = _interopRequireDefault(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _reactRouterDom = require("react-router-dom");

var _component = require("@loadable/component");

var _App = _interopRequireDefault(require("./components/App"));

(0, _component.loadableReady)(function () {
  _reactDom["default"].hydrate( /*#__PURE__*/_react["default"].createElement(_reactRouterDom.BrowserRouter, null, /*#__PURE__*/_react["default"].createElement(_App["default"], null)), document.getElementById('root'));
});