"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _server = require("react-dom/server");

var _reactRouterDom = require("react-router-dom");

var _App = _interopRequireDefault(require("./components/App"));

// import { ChunkExtractor, ChunkExtractorManager } from '@loadable/server';
var _default = function _default(extractor, path, context) {
  return (0, _server.renderToString)(extractor.collectChunks( /*#__PURE__*/_react["default"].createElement(_reactRouterDom.StaticRouter, {
    location: path,
    context: context
  }, /*#__PURE__*/_react["default"].createElement(_App["default"], null))));
};

exports["default"] = _default;