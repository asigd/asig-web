"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = Delta;

var _react = _interopRequireDefault(require("react"));

function Delta() {
  return /*#__PURE__*/_react["default"].createElement("section", {
    className: "main blur"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "page"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "content"
  }, /*#__PURE__*/_react["default"].createElement("h2", {
    className: "asigd-logo icon-asigd-logo"
  }, /*#__PURE__*/_react["default"].createElement("span", {
    className: "title"
  }, "Delta")), /*#__PURE__*/_react["default"].createElement("h3", null, "Development"), /*#__PURE__*/_react["default"].createElement("div", {
    className: "sections"
  }, /*#__PURE__*/_react["default"].createElement("section", null, /*#__PURE__*/_react["default"].createElement("h4", null, "Software development"), /*#__PURE__*/_react["default"].createElement("div", {
    className: "labels"
  }, /*#__PURE__*/_react["default"].createElement("span", null, "Technology agnostic"), /*#__PURE__*/_react["default"].createElement("span", null, "Abstraction layering"), /*#__PURE__*/_react["default"].createElement("span", null, "Full lifecycle"))), /*#__PURE__*/_react["default"].createElement("section", null, /*#__PURE__*/_react["default"].createElement("h4", null, "Product engineering"), /*#__PURE__*/_react["default"].createElement("div", {
    className: "labels"
  }, /*#__PURE__*/_react["default"].createElement("span", null, "Scoping"), /*#__PURE__*/_react["default"].createElement("span", null, "Prototyping"), /*#__PURE__*/_react["default"].createElement("span", null, "Executing"))), /*#__PURE__*/_react["default"].createElement("section", null, /*#__PURE__*/_react["default"].createElement("h4", null, "Information architecture"), /*#__PURE__*/_react["default"].createElement("div", {
    className: "labels"
  }, /*#__PURE__*/_react["default"].createElement("span", null, "Business intelligence"), /*#__PURE__*/_react["default"].createElement("span", null, "Data modelling"), /*#__PURE__*/_react["default"].createElement("span", null, "Analytics"))), /*#__PURE__*/_react["default"].createElement("section", null, /*#__PURE__*/_react["default"].createElement("h4", null, "Application modernization"), /*#__PURE__*/_react["default"].createElement("div", {
    className: "labels"
  }, /*#__PURE__*/_react["default"].createElement("span", null, "Serverless"), /*#__PURE__*/_react["default"].createElement("span", null, "Microservices"), /*#__PURE__*/_react["default"].createElement("span", null, "Horizontal scaling"))), /*#__PURE__*/_react["default"].createElement("section", null, /*#__PURE__*/_react["default"].createElement("h4", null, "Cloud transformation"), /*#__PURE__*/_react["default"].createElement("div", {
    className: "labels"
  }, /*#__PURE__*/_react["default"].createElement("span", null, "Distributed systems"), /*#__PURE__*/_react["default"].createElement("span", null, "High availability"), /*#__PURE__*/_react["default"].createElement("span", null, "Hybridization"))), /*#__PURE__*/_react["default"].createElement("section", null, /*#__PURE__*/_react["default"].createElement("h4", null, "Innovation"), /*#__PURE__*/_react["default"].createElement("div", {
    className: "labels"
  }, /*#__PURE__*/_react["default"].createElement("span", null, "Creative solutions"), /*#__PURE__*/_react["default"].createElement("span", null, "Market disruption"), /*#__PURE__*/_react["default"].createElement("span", null, "Emerging technologies")))))));
}