"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = Rho;

var _react = _interopRequireDefault(require("react"));

// import { Helmet } from 'react-helmet'
function Rho() {
  return /*#__PURE__*/_react["default"].createElement(_react["default"].Fragment, null, /*#__PURE__*/_react["default"].createElement("section", {
    className: "main blur"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "page"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "content"
  }, /*#__PURE__*/_react["default"].createElement("h2", {
    className: "asigd-logo icon-asigd-logo"
  }, /*#__PURE__*/_react["default"].createElement("span", {
    className: "title"
  }, "Rho")), /*#__PURE__*/_react["default"].createElement("h3", null, "Recruitment")))));
}