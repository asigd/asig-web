"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = Home;

var _react = _interopRequireDefault(require("react"));

// import StaticContext from '../contexts/StaticContext';
function Home() {
  // const context = useContext(StaticContext);
  // console.log('context', context);
  return /*#__PURE__*/_react["default"].createElement("section", {
    className: "main splash blur"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "hero"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "logo asigd-logo icon-asigd-logo"
  }), /*#__PURE__*/_react["default"].createElement("div", {
    className: "lead"
  }, /*#__PURE__*/_react["default"].createElement("h3", null, "Ad Omnes Ire Viam"))));
}