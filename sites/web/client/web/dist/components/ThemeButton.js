"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _typeof = require("@babel/runtime/helpers/typeof");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.PREF_SCHEME = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _SessionContext = _interopRequireWildcard(require("../contexts/SessionContext"));

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = (0, _getPrototypeOf2["default"])(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = (0, _getPrototypeOf2["default"])(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return (0, _possibleConstructorReturn2["default"])(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

var PREF_SCHEME = 'media';
exports.PREF_SCHEME = PREF_SCHEME;
var LIGHT_SCHEME = 'light';
var DARK_SCHEME = 'dark';
var COLOR_SCHEME_MQ = '(prefers-color-scheme)';
var DARK_SCHEME_MQ = '(prefers-color-scheme: dark)'; // const LIGHT_SCHEME_MQ = '(prefers-color-scheme: light)';

var ThemeButton = /*#__PURE__*/function (_React$Component) {
  (0, _inherits2["default"])(ThemeButton, _React$Component);

  var _super = _createSuper(ThemeButton);

  function ThemeButton(props, context) {
    var _this;

    (0, _classCallCheck2["default"])(this, ThemeButton);
    _this = _super.call(this, props);
    _this.state = {
      theme: context.state.theme
    };
    _this.themeChange = _this.themeChange.bind((0, _assertThisInitialized2["default"])(_this));
    return _this;
  }

  (0, _createClass2["default"])(ThemeButton, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.colorSchemeMediaQuery = window.matchMedia(COLOR_SCHEME_MQ);
      this.darkSchemeMediaQuery = window.matchMedia(DARK_SCHEME_MQ);
      this.hasColorSchemeSupport = this.colorSchemeMediaQuery.media !== 'not all';
      var state = (0, _SessionContext.getStoredState)();

      if (state && state.theme) {
        this.setState({
          theme: state.theme
        });
      }
    }
  }, {
    key: "themeChange",
    value: function themeChange() {
      var _this2 = this;

      var theme = PREF_SCHEME;

      switch (this.state.theme) {
        case LIGHT_SCHEME:
          theme = DARK_SCHEME;
          break;

        case DARK_SCHEME:
          theme = LIGHT_SCHEME;
          break;

        case PREF_SCHEME:
          theme = this.darkSchemeMediaQuery.matches ? LIGHT_SCHEME : DARK_SCHEME;
          break;
      }

      this.setState({
        theme: theme
      }, function () {
        _this2.context.setState(_this2.state);
      });
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react["default"].createElement("button", {
        type: "button",
        className: "icon-button",
        "aria-label": "Toggle theme",
        title: "Toggle theme",
        onClick: this.themeChange
      }, /*#__PURE__*/_react["default"].createElement("i", {
        className: "fas fa-adjust"
      }));
    }
  }]);
  return ThemeButton;
}(_react["default"].Component);

(0, _defineProperty2["default"])(ThemeButton, "contextType", _SessionContext["default"]);
var _default = ThemeButton;
exports["default"] = _default;