"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _react = _interopRequireDefault(require("react"));

var _ThemeButton = _interopRequireDefault(require("./ThemeButton"));

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = (0, _getPrototypeOf2["default"])(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = (0, _getPrototypeOf2["default"])(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return (0, _possibleConstructorReturn2["default"])(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

var Layout = /*#__PURE__*/function (_React$Component) {
  (0, _inherits2["default"])(Layout, _React$Component);

  var _super = _createSuper(Layout);

  function Layout() {
    (0, _classCallCheck2["default"])(this, Layout);
    return _super.apply(this, arguments);
  }

  (0, _createClass2["default"])(Layout, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react["default"].createElement(_react["default"].Fragment, null, /*#__PURE__*/_react["default"].createElement("div", {
        className: "backdrop blur"
      }), this.props.children, /*#__PURE__*/_react["default"].createElement("header", {
        className: "flex-rows blur"
      }, /*#__PURE__*/_react["default"].createElement(_ThemeButton["default"], null), /*#__PURE__*/_react["default"].createElement("button", {
        type: "button",
        "data-consent-toggle": true,
        className: "icon-button",
        "aria-label": "Cookies settings",
        title: "Cookies settings"
      }, /*#__PURE__*/_react["default"].createElement("i", {
        className: "fas fa-cookie-bite"
      }))), /*#__PURE__*/_react["default"].createElement("footer", {
        className: "flex-rows blur"
      }, /*#__PURE__*/_react["default"].createElement("a", {
        className: "icon-button",
        href: "https://asig.uk/info",
        title: "Meet the pack"
      }, /*#__PURE__*/_react["default"].createElement("i", {
        className: "fas fa-info"
      })), /*#__PURE__*/_react["default"].createElement("a", {
        className: "icon-button",
        href: "https://asig.uk/email",
        title: "Join the hunt"
      }, /*#__PURE__*/_react["default"].createElement("i", {
        className: "fas fa-envelope"
      })), /*#__PURE__*/_react["default"].createElement("a", {
        className: "icon-button",
        href: "https://asig.uk/call",
        title: "Howl a call"
      }, /*#__PURE__*/_react["default"].createElement("i", {
        className: "fas fa-phone"
      })), /*#__PURE__*/_react["default"].createElement("a", {
        className: "icon-button",
        href: "https://asig.uk/in",
        title: "Follow the tracks"
      }, /*#__PURE__*/_react["default"].createElement("i", {
        className: "fab fa-linkedin-in"
      }))));
    }
  }]);
  return Layout;
}(_react["default"].Component);

var _default = Layout;
exports["default"] = _default;