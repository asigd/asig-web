"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.SessionContext = exports.setStoredState = exports.getStoredState = void 0;

var _react = require("react");

var STORAGE_KEY = 'state';

var getStoredState = function getStoredState() {
  try {
    return JSON.parse(window.sessionStorage.getItem(STORAGE_KEY));
  } catch (error) {
    return null;
  }
};

exports.getStoredState = getStoredState;

var setStoredState = function setStoredState(state) {
  window.sessionStorage.setItem(STORAGE_KEY, JSON.stringify(state));
};

exports.setStoredState = setStoredState;
var SessionContext = /*#__PURE__*/(0, _react.createContext)({
  state: {},
  setState: function setState() {}
});
exports.SessionContext = SessionContext;
var _default = SessionContext;
exports["default"] = _default;