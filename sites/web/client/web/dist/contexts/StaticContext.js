"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.StaticContext = void 0;

var _react = require("react");

var StaticContext = /*#__PURE__*/(0, _react.createContext)({
  routes: {},
  route: {}
});
exports.StaticContext = StaticContext;
var _default = StaticContext;
exports["default"] = _default;