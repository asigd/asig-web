"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _default = {
  "title": "Alpha Sigma",
  "description": "Neoteric software house and disruptive locomotive, with one purpose.",
  "slogan": "Go all the way.",
  "logo": "https://s.asig.uk/logo/asig-logo-220-fd922cfd24b45dfb9a25.png"
};
exports["default"] = _default;