<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" version="5" doctype-system="about:legacy-compat" encoding="UTF-8"/>
  <xsl:template match="/">
    <html lang="en">
    <head>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title><xsl:value-of select="error/message"/></title>
      <style>
<![CDATA[
html,
body,
h1,
h2,
pre,
header,
footer,
nav {
  padding: 0;
  margin: 0;
}

body {
  line-height: 1;
  font-size: 100%;
  text-align: center;
  font-family: monospace;
}

h1,
h2 {
  font-weight: bold;
}

h1 {
  font-size: 32px;
}

h2 {
  font-size: 24px;
}

pre {
  display: inline-block;
  text-align: left;
  white-space: pre;
  font-size: 12px;
}

header {
  padding-top: 4em;
  margin-bottom: 4em;
}

footer {
  margin-top: 4em;
  padding-bottom: 4em;
}

header > * + * {
  margin-top: 1em;
}
]]>
      </style>
      <style>
<![CDATA[
body {
  background-color: #fff;
  color: #000;
  opacity: 0.8;
}
]]>
      </style>
    </head>
    <body>
      <xsl:apply-templates/>
    </body>
    </html>
  </xsl:template>
  <xsl:template match="error">
    <header>
      <h2><xsl:value-of select="code"/></h2>
      <h1><xsl:value-of select="message"/></h1>
    </header>
    <pre>
      <xsl:choose>
        <xsl:when test="stack">
          <xsl:value-of select="stack"/>
        </xsl:when>
        <xsl:otherwise>
<![CDATA[
                              __
                            .d$$b
                          .' TO$;\
                         /  : TP._;
                        / _.;  :Tb|
                       /   /   ;j$j
                   _.-"       d$$$$
                 .' ..       d$$$$;
                /  /P'      d$$$$P. |\
               /   "      .d$$$P' |\^"l
             .'           `T$P^"""""  :
         ._.'      _.'                ;
      `-.-".-'-' ._.       _.-"    .-"
    `.-" _____  ._              .-"
   -(.g$$$$$$$b.              .'
     ""^^T$$$P^)            .(:
       _/  -"  /.'         /:/;
    ._.'-'`-'  ")/         /;/;
 `-.-"..--""   " /         /  ;
.-" ..--""        -'          :
..--""--.-"         (\      .-(\
  ..--""              `-\(\/;`
    _.                      :
                            ;`-
                           :\
                           ;  bug
]]>
        </xsl:otherwise>
      </xsl:choose>
    </pre>
    <footer>
      <nav><a href="/">Go Home</a></nav>
    </footer>
  </xsl:template>
</xsl:stylesheet>