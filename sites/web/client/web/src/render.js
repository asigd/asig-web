import React from 'react';
import { renderToString } from 'react-dom/server';
import { StaticRouter } from 'react-router-dom';
// import { ChunkExtractor, ChunkExtractorManager } from '@loadable/server';
import App from './components/App';

export default (extractor, path, context) => {
    return renderToString(
        extractor.collectChunks(
            <StaticRouter location={path} context={context}>
                <App />
            </StaticRouter>
        )
    );
};