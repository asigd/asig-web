import React from 'react'
// import { Helmet } from 'react-helmet'

import '../assets/styles/pages.scss';

export default function Rho() {
    return (
        <React.Fragment>
            <section className="main blur">
                <div className="page">
                    <div className="content">
                        <h2 className="asigd-logo icon-asigd-logo"><span className="title">Rho</span></h2>
                        <h3>Recruitment</h3>
                    </div>
                </div>
            </section>
        </React.Fragment>
    );
}