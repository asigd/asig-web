import React from 'react'
// import StaticContext from '../contexts/StaticContext';

import '../assets/styles/pages.scss';
import '../assets/styles/pages/home.scss';

export default function Home() {

    // const context = useContext(StaticContext);
    // console.log('context', context);

    return (
        <section className="main splash blur">
            <div className="hero">
                <div className="logo asigd-logo icon-asigd-logo"></div>
                <div className="lead">
                    <h3>Ad Omnes Ire Viam</h3>
                </div>
            </div>
        </section>
    );
}