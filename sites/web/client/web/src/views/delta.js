import React from 'react'

import '../assets/styles/pages.scss';

export default function Delta() {
    return (
        <section className="main blur">
            <div className="page">
                <div className="content">
                    <h2 className="asigd-logo icon-asigd-logo"><span className="title">Delta</span></h2>
                    <h3>Development</h3>
                    <div className="sections">
                        <section>
                            <h4>Software development</h4>
                            <div className="labels">
                                <span>Technology agnostic</span>
                                <span>Abstraction layering</span>
                                <span>Full lifecycle</span>
                            </div>
                        </section>
                        <section>
                            <h4>Product engineering</h4>
                            <div className="labels">
                                <span>Scoping</span>
                                <span>Prototyping</span>
                                <span>Executing</span>
                            </div>
                        </section>
                        <section>
                            <h4>Information architecture</h4>
                            <div className="labels">
                                <span>Business intelligence</span>
                                <span>Data modelling</span>
                                <span>Analytics</span>
                            </div>
                        </section>
                        <section>
                            <h4>Application modernization</h4>
                            <div className="labels">
                                <span>Serverless</span>
                                <span>Microservices</span>
                                <span>Horizontal scaling</span>
                            </div>
                        </section>
                        <section>
                            <h4>Cloud transformation</h4>
                            <div className="labels">
                                <span>Distributed systems</span>
                                <span>High availability</span>
                                <span>Hybridization</span>
                            </div>
                        </section>
                        <section>
                            <h4>Innovation</h4>
                            <div className="labels">
                                <span>Creative solutions</span>
                                <span>Market disruption</span>
                                <span>Emerging technologies</span>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </section>
    );
}