import { createContext } from 'react';

const STORAGE_KEY = 'state';

export const getStoredState = () => {
    try {
        return JSON.parse(window.sessionStorage.getItem(STORAGE_KEY));
    } catch (error) { 
        return null;
    }
}

export const setStoredState = (state) => {
    window.sessionStorage.setItem(STORAGE_KEY, JSON.stringify(state));
}

export const SessionContext = createContext({ state: {}, setState: () => { } });

export default SessionContext;