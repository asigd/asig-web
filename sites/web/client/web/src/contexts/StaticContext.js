import { createContext } from 'react';

export const StaticContext = createContext({ routes: {}, route: {} });

export default StaticContext;