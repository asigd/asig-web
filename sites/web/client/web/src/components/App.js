import React from 'react';
import { Helmet } from 'react-helmet';
import { withRouter, matchPath } from 'react-router-dom';
import SessionContext, { getStoredState, setStoredState } from '../contexts/SessionContext';
import StaticContext from '../contexts/StaticContext';
import routes from '../routes';
import Layout from './Layout';
import { PREF_SCHEME } from './ThemeButton';
import meta from '../meta.config';

import '../assets/styles/layout.scss';
import(/* webpackPreload: true */ '../assets/css/dialog.css');
import(/* webpackPreload: true */ '../assets/css/asigd-logo.css');
import(/* webpackPreload: true */ '@fortawesome/fontawesome-free/css/all.min.css');
// import('bootstrap/dist/css/bootstrap.min.css');

class App extends React.Component {

    constructor(props) {
        super(props);

        this.state = { theme: PREF_SCHEME };
    }

    componentDidMount() {
        const state = getStoredState();
        if (state) {
            this.setState(state);
        }
    }

    render() {

        const route = routes.find((route) => {
            return matchPath(this.props.location.pathname, route);
        });
        const data = route.data || {};

        return (
            <SessionContext.Provider value={{
                state: this.state,
                setState: (state) => {
                    this.setState({ ...this.state, ...state }, () => {
                        setStoredState(this.state);
                    });
                }
            }}>
                <StaticContext.Provider value={{ routes, route }}>
                    <Helmet>
                        <html lang="en" />
                        <body data-color-scheme={this.state.theme} />
                        <title>{data.title}</title>
                        <meta name="description" content={meta.description} />
                        <meta property="og:title" content={data.title} />
                        <meta property="og:description" content={meta.slogan} />
                        <meta property="og:image" content={meta.logo} />
                    </Helmet>
                    <Layout>
                        <route.component />
                    </Layout>
                </StaticContext.Provider>
            </SessionContext.Provider>
        );
    }
}

export default withRouter(App);
