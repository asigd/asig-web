import React from 'react';
import SessionContext, { getStoredState } from '../contexts/SessionContext';

export const PREF_SCHEME = 'media';
const LIGHT_SCHEME = 'light';
const DARK_SCHEME = 'dark';
const COLOR_SCHEME_MQ = '(prefers-color-scheme)';
const DARK_SCHEME_MQ = '(prefers-color-scheme: dark)';
// const LIGHT_SCHEME_MQ = '(prefers-color-scheme: light)';

class ThemeButton extends React.Component {

    static contextType = SessionContext;

    constructor(props, context) {
        super(props);

        this.state = { theme: context.state.theme };

        this.themeChange = this.themeChange.bind(this);
    }

    componentDidMount() {

        this.colorSchemeMediaQuery = window.matchMedia(COLOR_SCHEME_MQ);
        this.darkSchemeMediaQuery = window.matchMedia(DARK_SCHEME_MQ);
        this.hasColorSchemeSupport = (this.colorSchemeMediaQuery.media !== 'not all');

        const state = getStoredState();
        if (state && state.theme) {
            this.setState({ theme: state.theme });
        }
    }

    themeChange() {

        let theme = PREF_SCHEME;

        switch (this.state.theme) { 
            case LIGHT_SCHEME:
                theme = DARK_SCHEME;
                break;
            case DARK_SCHEME:
                theme = LIGHT_SCHEME;
                break;
            case PREF_SCHEME:
                theme = (this.darkSchemeMediaQuery.matches) ? LIGHT_SCHEME : DARK_SCHEME;
                break;
        }

        this.setState({ theme }, () => {
            this.context.setState(this.state);
        });
    }

    render() {
        return (
            <button type="button" className="icon-button" aria-label="Toggle theme" title="Toggle theme" onClick={this.themeChange}>
                <i className="fas fa-adjust"></i>
            </button>
        );
    }
}

export default ThemeButton;