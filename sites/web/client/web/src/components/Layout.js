import React from 'react';
import ThemeButton from './ThemeButton';

class Layout extends React.Component {

    render() {
        return (
            <React.Fragment>
                <div className="backdrop blur"></div>

                {this.props.children}

                <header className="flex-rows blur">
                    <ThemeButton />
                    <button type="button" data-consent-toggle className="icon-button" aria-label="Cookies settings" title="Cookies settings">
                        <i className="fas fa-cookie-bite"></i>
                    </button>
                </header>

                <footer className="flex-rows blur">
                    <a className="icon-button" href="https://asig.uk/info" title="Meet the pack">
                        <i className="fas fa-info"></i>
                    </a>
                    <a className="icon-button" href="https://asig.uk/email" title="Join the hunt">
                        <i className="fas fa-envelope"></i>
                    </a>
                    <a className="icon-button" href="https://asig.uk/call" title="Howl a call">
                        <i className="fas fa-phone"></i>
                    </a>
                    <a className="icon-button" href="https://asig.uk/in" title="Follow the tracks">
                        <i className="fab fa-linkedin-in"></i>
                    </a>
                </footer>
            </React.Fragment>
        );
    }
}

export default Layout;