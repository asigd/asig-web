import loadable from '@loadable/component';

const Home = loadable(() => import('./views/home'));
const Delta = loadable(() => import('./views/delta'));
const Rho = loadable(() => import('./views/rho'));

const routes = [
    {
        name: 'home',
        path: '/',
        exact: true,
        component: Home,
        data: {
            title: "Alpha Sigma",
        },
    },
    {
        name: 'delta',
        path: '/delta',
        component: Delta,
        data: {
            title: "Alpha Sigma Delta",
        },
    },
    {
        name: 'rho',
        path: '/rho',
        component: Rho,
        data: {
            title: "Alpha Sigma Rho",
        },
    }
];

export default routes;
