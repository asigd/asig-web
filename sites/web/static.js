#!/usr/bin/env node

// const fs = require('fs')
const fs = require('fs').promises
const path = require('path')
// const assert = require('assert')
const yargs = require('yargs/yargs')

// const handlebars = require('handlebars')
const nunjucks = require('nunjucks')
const cheerio = require('cheerio')
const minify = require('html-minifier-terser').minify
const beautify = require('js-beautify').html

const Helmet = require('react-helmet').Helmet
const ChunkExtractor = require('@loadable/server').ChunkExtractor

const args = yargs(process.argv.slice(2)).argv
args.env = args.env.reduce((obj, val) => {
    const pair = String(val).split('=')
    return {...obj, [pair[0]]: pair[1]}
}, {})

const webpack = require('./webpack.config')(args.env)

const time = new Date().getTime()
const statsFile = path.resolve('./loadable-stats.json')
const srcFile = path.resolve(webpack.output.path, 'index.html')
const inputFile = `${srcFile}~${time}`

const routes = require(args.env.routes).default
const render = require(path.resolve(args._[0], 'render')).default


// TODO: ==> @jslabs/react-static, @jslabs/react-ssr ...
const renderStatic = async () => {

    await fs.copyFile(srcFile, inputFile)

    for (let route of routes) {

        const outputFile = path.resolve(webpack.output.path, route.path.replace(/^\//, ''), 'index.html')
        const extractor = new ChunkExtractor({ statsFile, /* entrypoints: ['main'] */ })
        let dom = render(extractor, route.path)
        // dom = beautify(dom, { inline: [] })

        let helmet = Helmet.renderStatic()
        for (const key in helmet) {
            helmet[key] = helmet[key].toString()
        }

        // let styleFiles = []
        // const $styleTags = cheerio.load(extractor.getStyleTags())
        // $styleTags('link[data-chunk]').each((i, elem) => {
        //     styleFiles[i] = $styleTags(elem).attr('href')
        // })
        // console.log(styleFiles)

        const $linkTags = cheerio.load(extractor.getLinkTags())
        $linkTags('link[data-chunk][as=style]').remove()
        let linkTags = $linkTags.html($linkTags('link'))
        linkTags = beautify(linkTags)

        const [inlineStyleTags, cssString] = await Promise.all([extractor.getInlineStyleTags(), extractor.getCssString()])

        const chunks = {
            'linkTags': linkTags,
            'scriptTags': extractor.getScriptTags(),
            'styleTags': extractor.getStyleTags(),
            'inlineStyleTags': inlineStyleTags,
            'cssString': cssString,
        }

        const template = await fs.readFile(inputFile, { encoding: 'utf8', flag: 'r' })
        let markup = nunjucks.renderString(template, { dom, helmet, chunks })

        // console.log('style....................', chunks.styleTags)
        // console.log('chunks........................................' + route.path, chunks.linkTags)
        // console.log('template..................................', template)

        markup = await minify(markup, {
            collapseWhitespace: true,
            preserveLineBreaks: true,
            removeComments: true,
            minifyCSS: true,
            minifyJS: true,
            removeStyleLinkTypeAttributes: true,
            removeScriptTypeAttributes: true,
        })

        try {
            await fs.mkdir(path.dirname(outputFile))
        } catch (error) {
            //
        }

        await fs.writeFile(outputFile, markup, { encoding: 'utf8', flag: 'w' })

    }

    fs.unlink(inputFile)

}

renderStatic()