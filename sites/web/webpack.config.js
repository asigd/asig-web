const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const LoadablePlugin = require('@loadable/webpack-plugin')

module.exports = (env, argv) => {

    if (process.env.NODE_ENV) {
        env[process.env.NODE_ENV] = true
    }

    return {
        entry: './',
        context: path.resolve(__dirname, env.context),
        output: {
            filename: 'assets/js/[name].[chunkhash].js',
            chunkFilename: 'assets/js/[name].[chunkhash].js',
            path: path.resolve(__dirname, 'public'),
            publicPath: '/',
            assetModuleFilename: 'assets/[name].[hash][ext][query]',
        },
        devtool: (env.production) ? false : 'eval-source-map',
        optimization: {
            splitChunks: {
                chunks: 'all',
            }
        },
        module: {
            rules: [
                {
                    test: /\.js(x?)$/,
                    exclude: /(node_modules)/,
                    use: {
                        loader: 'babel-loader',
                    },
                },
                {
                    test: /\.(jp(e?)g|png|gif)$/,
                    type: 'asset/resource',
                    generator: {
                        filename: 'assets/images/[name].[hash][ext][query]',
                    }
                },
                {
                    test: /\.(eot|woff(2?)|svg|ttf)$/,
                    type: 'asset/resource',
                    generator: {
                        filename: 'assets/fonts/[name].[hash][ext][query]',
                    }
                },
                {
                    test: /\.s?css$/,
                    use: [
                        {
                            // loader: 'style-loader',
                            loader: (env.production) ? MiniCssExtractPlugin.loader : 'style-loader',
                        },
                        {
                            loader: 'css-loader',
                        },
                        {
                            loader: 'sass-loader',
                        },
                    ],
                },
            ]
        },
        plugins: [
            new HtmlWebpackPlugin({
                template: './templates/index.html',
                filename: 'index.html',
                inject: false,
                cache: true,
                minify: false,
            }),
            new MiniCssExtractPlugin({
                filename: 'assets/css/[name].[chunkhash].css',
                chunkFilename: 'assets/css/[name].[chunkhash].css',
            }),
            new LoadablePlugin({ outputAsset: false, writeToDisk: { filename: './' } }),
        ]
    }
}